class Fixnum
  def in_words
    number = self
    number_in_words = []

    return "zero" if number == 0

    digit = 0
    while number!=0

      digit_in_word = ""

      case digit%3

        when 0
          if number%100 < 20 && number%100 > 9
            digit_in_word = teens_in_words(number%100)
            digit+=1
            number/=10

          else
            digit_in_word = singles_in_words(number%10)

          end

          multiple_in_words(digit,number_in_words) unless number%1000 == 0

        when 1
          digit_in_word = tens_in_words(number%10)

        when 2
          digit_in_word = "#{singles_in_words(number%10)} hundred"
      end

      number_in_words.unshift(digit_in_word) unless number%10==0
      digit+=1
      number/=10
    end


    number_in_words.join(" ")
  end

  def multiple_in_words(digit,number_in_words)
    case digit/3
      when 1
        number_in_words.unshift("thousand")
      when 2
        number_in_words.unshift("million")
      when 3
        number_in_words.unshift("billion")
      when 4
        number_in_words.unshift("trillion")
    end
  end

  def singles_in_words(int)
    word=""
    case int
      when 1
        word = "one"
      when 2
        word = "two"
      when 3
        word = "three"
      when 4
        word = "four"
      when 5
        word = "five"
      when 6
        word = "six"
      when 7
        word = "seven"
      when 8
        word = "eight"
      when 9
        word = "nine"
    end
    word
  end

  def teens_in_words(int)
    word=""
    case int
      when 10
        word = "ten"
      when 11
        word = "eleven"
      when 12
        word = "twelve"
      when 13
        word = "thirteen"
      when 14
        word = "fourteen"
      when 15
        word = "fifteen"
      when 16
        word = "sixteen"
      when 17
        word = "seventeen"
      when 18
        word = "eighteen"
      when 19
        word = "nineteen"
    end
    word
  end

  def tens_in_words(int)
    word=""
    case int
      when 2
        word = "twenty"
      when 3
        word = "thirty"
      when 4
        word = "forty"
      when 5
        word = "fifty"
      when 6
        word = "sixty"
      when 7
        word = "seventy"
      when 8
        word = "eighty"
      when 9
        word = "ninety"
    end
    word
  end
end
